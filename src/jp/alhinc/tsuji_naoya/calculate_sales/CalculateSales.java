package jp.alhinc.tsuji_naoya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;


public class CalculateSales {
	public static void main(String[] args) {

		if (args.length !=1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		HashMap<String, String> branchNameMap = new HashMap<String, String>();
		HashMap<String, Long> profitMap = new HashMap<String, Long>();

		BufferedReader br = null;

		if (!readFile(args[0], "branch.lst", branchNameMap, profitMap, "^[0-9]{3}$", "支店")) {
			return;
		}

		File dir = new File(args[0]);
		File[] files = dir.listFiles();

		ArrayList<String> revenueList = new ArrayList<String>();
		ArrayList<Integer> fileList = new ArrayList<Integer>();

		for (int i = 0; i< files.length; i++) {
				String st = files[i].getName();
				if (files[i].isFile() && st.matches("^[0-9]{8}.rcd$")) {
					String newString = st.substring(0, 8);
					Integer fileInteger = Integer.parseInt(newString);
					fileList.add(fileInteger);
					revenueList.add(st);
				}
		}

		for (int i = 0; i <fileList.size()-1; i++) {
			if ((fileList.get(i+1)) - fileList.get(i) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		for (int i = 0; i < revenueList.size(); i++) {
			try {
				ArrayList<String> salesList = new ArrayList<String>();
				File file = new File(args[0], revenueList.get(i));
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line;
				while ((line = br.readLine()) != null) {
					salesList.add(line);
				}

				if (salesList.size() != 2) {
					System.out.println(revenueList.get(i) + "のフォーマットが不正です");
					return;
				}

				if (!branchNameMap.containsKey(salesList.get(0))) {
					System.out.println(revenueList.get(i) + "の支店コードが不正です");
					return;
				}

				if (!(salesList.get(1)).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				Long lon = Long.parseLong(salesList.get(1));
				Long sum = lon + profitMap.get(salesList.get(0));

				if (sum >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				profitMap.put(salesList.get(0), sum);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		if (!writeFile(args[0], branchNameMap, profitMap, "branch.out")) {
			return;
		}
	}

	public static boolean writeFile (String path, HashMap<String, String> NameMap, HashMap<String, Long> profitMap, String fileName) {

		BufferedWriter bw = null;
		String sep = System.getProperty("line.separator");
		try {
			File fileout = new File(path, fileName);
			FileWriter fw = new FileWriter(fileout);
			bw = new BufferedWriter(fw);

			for (Entry<String, String> entry: NameMap.entrySet()) {
				bw.write(entry.getKey() + "," + entry.getValue() + "," + profitMap.get(entry.getKey()) + sep);
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	public static boolean readFile (String path, String fileName, HashMap<String, String> NameMap, HashMap<String, Long> profitMap, String matchNumber, String definition) {

		BufferedReader br = null;
		try {
			File file = new File(path, fileName);

			if (!file.exists()) {
				System.out.println(definition + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {
				String[] branches = line.split(",");

				if (branches.length != 2) {
					System.out.println(definition + "定義ファイルのフォーマットが不正です");
					return false;
				}

				if (!branches[0].matches(matchNumber)) {
					System.out.println(definition + "定義ファイルのフォーマットが不正です");
					return false;
				}

				NameMap.put(branches[0], branches[1]);
				profitMap.put(branches[0], 0L);
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
