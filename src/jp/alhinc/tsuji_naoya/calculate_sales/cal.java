package jp.alhinc.tsuji_naoya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;





public class cal {

	public static void main(String[] args) {
		HashMap<String, String> branchNameMap = new HashMap<String, String>();
		HashMap<String,Long> profitMap = new HashMap<String,Long>();



		BufferedReader br = null;
		try{

			File file = new File(args[0], "branch.lst");

			if (!file.exists()){
				System.out.println("支店定義ファイルが存在しません");
				return;
			}


			FileReader fr = new FileReader(file);

//			ここにエラー検索をいれると反応しない。
//			なぜならFileReaderした時点でファイルは存在すると取られる

			br = new BufferedReader(fr);


			if (!file.exists()){
				System.out.println("支店定義ファイルが存在しません");
			}


			String line;
			while((line = br.readLine()) != null){

				String[] branches = line.split(",");

				if(branches.length != 2){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}


				branchNameMap.put(branches[0], branches[1]);
				profitMap.put(branches[0], 0L);


				if(!branches[0].matches("^[0-9]{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;

				}
			}
		}catch(IOException e){
			System.out.println("エラー1が発生");
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした");
				}

			}
		}

		File dir = new File(args[0]);
		File[] files = dir.listFiles();

		ArrayList<String> bigfile = new ArrayList<String>();
		for (int i =  0 ; i< files.length; i++){
			String st  =files[i].getName();
			if (st.matches("^[0-9]{8}.rcd$") ){
				
				
				String newstring = st.substring(0,8);
				Long newlong = Long.parseLong(newstring);
//				Arrays.sort(newlong, Comparator.naturalOrder());
				
				System.out.println(newlong);
				
				
				
				bigfile.add(st);
			}
		}
		
		
		

		

		
		
		
		


		/////////////////////////////////////////////////////////////////////////






		for(int i = 0 ;i < bigfile.size() ; i++) {
			try{
				ArrayList<String> keyvalue = new ArrayList<String>();
				File file = new File(args[0], bigfile.get(i));

				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);



				String line;
				while((line = br.readLine()) != null){
					keyvalue.add(line);

//					この時点ではkeyvalue(0)しか作られていない
//					Long lon = Long.parseLong(keyvalue.get(1));　　←（1）は作成されていない

//					String[] profit = line.split(",");　←splitする必要はない。もともとが二行になっているから。

				}

				Long lon = Long.parseLong(keyvalue.get(1));
				Long sum = lon + profitMap.get(keyvalue.get(0));
				if(sum > 1000000000L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				profitMap.put(keyvalue.get(0),sum);


			}catch(IOException e){
				System.out.println("エラー2が発生");
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("closeできませんでした");
					}

				}
			}

		}

		BufferedWriter bw = null;
		try{

			File fileout = new File(args[0],"branch.out");
			FileWriter fw = new FileWriter(fileout);
			bw = new BufferedWriter(fw);

			for(Entry<String, String> entry: branchNameMap.entrySet()){
				System.out.println(entry.getKey() + "," + entry.getValue() + "," + profitMap.get(entry.getKey()) + "\n");
				bw.write(entry.getKey() + "," + entry.getValue() + "," + profitMap.get(entry.getKey()) + "\n");
				}

		}catch(IOException e){
			System.out.println("エラー3が発生");
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした");
				}

			}
		}

	}
}
